﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VideoForch
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private string FileName;
        private List<TimeSpan> positionList = new List<TimeSpan>();
        public MainWindow()
        {
            InitializeComponent();
        }
        
        void OnMouseDownPlayMedia(object sender, MouseButtonEventArgs args)
        {

            myMediaElement.Play();


        }


        void OnMouseDownPauseMedia(object sender, MouseButtonEventArgs args)
        {

            myMediaElement.Pause();
        }


        void OnMouseDownStopMedia(object sender, MouseButtonEventArgs args)
        {

            myMediaElement.Stop();
        }



        private void Element_MediaOpened(object sender, EventArgs e)
        {
            if (myMediaElement.NaturalDuration.HasTimeSpan) {
                timelineSlider.Maximum = myMediaElement.NaturalDuration.TimeSpan.TotalMilliseconds; }
        }


        private void Element_MediaEnded(object sender, EventArgs e)
        {
            myMediaElement.Stop();
        }

        // Jump to different parts of the media (seek to). 
        private void SeekToMediaPosition(object sender, RoutedPropertyChangedEventArgs<double> args)
        {
            int SliderValue = (int)timelineSlider.Value;

            TimeSpan ts = new TimeSpan(0, 0, 0, 0, SliderValue);
            myMediaElement.Position = ts;
        }


        private void Button_Click_PlayFile(object sender, RoutedEventArgs e)
        {
            myMediaElement.Play();

        }

        private void Button_Click_OpenFile(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDlg = new OpenFileDialog();

            Nullable<bool> result = openFileDlg.ShowDialog();
            if (result == true)
            {
                FileName = openFileDlg.FileName;
                TextBlockSelectedFile.Text = System.IO.File.ReadAllText(openFileDlg.FileName);
                myMediaElement.Source = new Uri(FileName);
            }
        }

        private void Button_Click_SavePosition(object sender, RoutedEventArgs e)
        {
            positionList.Add(myMediaElement.Position);

        }
    }
}
